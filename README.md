# My instance sonarqube

---

## Testing AWS EC2 Debian 10

---

### Installation des dependances

- Executer cette commande

    ```bash
    sudo apt-get install git
    ```

- Cloner les sources

    ```bash
    git clone https://gitlab.com/Metal65/sonar-instance.git
    ```

- Change shell

    ```bash
    sudo apt-get install zsh
    **how-to-install-java-with-apt-on-debian-10**`
    ```

- Telecharger le script
  
    [sonar-script.sh](https://gitlab.com/Metal65/sonarqube-8.1)

---

## Testing AWS EC2 linux Ubuntu

---

### Installation des dependances

- Executer cette commande

     ```bash
    sudo apt-get update
    sudo apt-get upgrade
    ```

    ```bash
    sudo yum install git
    ```

- Cloner les sources

    ```bash
    git clone https://gitlab.com/Metal65/sonar-instance.git
    ```

- Change shell

    ```bash
    sudo apt-get install zsh
    ```

    **Telecharger le script**
  
    [dependance.sh](https://gitlab.com/Metal65/sonarqube-8.1)

---

**how-to-install-java-with-apt-on-debian-10**`

<https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-debian-10>

/Users/laurent/Documents/sonar-scanner/bin
/Users/laurent/Documents/SonarQube/bin/macosx-universal-64

/Users/laurent/Documents/sonar-scanner/bin/sonar-scanner

### Installer java

https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04